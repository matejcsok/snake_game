const scl = 20;
let snake;
let food;

function setup() {
    createCanvas(500, 500);

    snake = new Snake();
    frameRate(10);
    foodLocation();
    noLoop();
}

function draw() {
    background(51);
    snake.show();
    snake.update();

    if (snake.eat(food)) {
        snake.total++;
        foodLocation();
    }

    fill(255, 0, 100);
    rect(food.x, food.y, scl, scl);
    fill(255);
    document.getElementById('total').innerText = 'Total: ' + (snake.total + 1);

    snake.die();
    snake.jump();
    console.log(snake.x, snake.y);
}

function foodLocation() {
    let col = floor(width / scl);
    let row = floor(height / scl);
    food = createVector(floor(random(col)), floor(random(row)));
    food.mult(scl);
}

function keyPressed() {
    switch (keyCode) {
        case UP_ARROW:
            snake.dir(0, -1);
            break;
        case DOWN_ARROW:
            snake.dir(0, 1);
            break;
        case RIGHT_ARROW:
            snake.dir(1, 0);
            break;
        case LEFT_ARROW:
            snake.dir(-1, 0);
            break;
        case ENTER:
            loop();
            break;
        case SHIFT:
            noLoop();
            break;
    }
}