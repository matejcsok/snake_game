function Snake() {
    this.x = 20;
    this.y = 20;
    this.xSpeed = 1;
    this.ySpeed = 0;
    this.total = 0;
    this.tail = [];

    this.update = function () {
        if (this.total === this.tail.length) {
            for (let index = 0; index < this.tail.length - 1; index++) {
                this.tail[index] = this.tail[index + 1];
            }
        }
        this.tail[this.total - 1] = createVector(this.x, this.y);


        this.x += this.xSpeed * scl;
        this.y += this.ySpeed * scl;

        this.x = constrain(this.x, -scl, width);
        this.y = constrain(this.y, -scl, height);
    };

    this.show = function () {
        fill(255);

        for (let index = 0; index < this.tail.length; index++) {
            rect(this.tail[index].x, this.tail[index].y, scl, scl);
        }

        rect(this.x, this.y, scl, scl);
    };

    this.dir = function (x, y) {
        this.xSpeed = x;
        this.ySpeed = y;
    };

    this.eat = function (pos) {
        return dist(this.x, this.y, pos.x, pos.y) < 1;
    };

    this.die = () => {
        this.tail.map(item => {
            if (dist(this.x, this.y, item.x, item.y) < 1) {
                this.total = 0;
                this.tail = [];
            }
        });

    };

    this.jump = () => {
        if (this.x === width) {
            this.x = 0;
        } else if (this.x+scl === 0) {
            this.x = width;
        }
        if (this.y === height) {
            this.y = 0;
        } else if (this.y+scl === 0) {
            this.y = height;
        }
    };

}